# Jaunt-lighthouse

# 개요
Traccar 로 인해 위치 데이터가 수집되고 있는 DB를 모니터링하고 조건에 따라 부가적인 서비스를 제공하는 프로그램입니다.

# 구조
```
├── dbhome
├── dev.server
│   ├── dbhome
│   ├── pghome
│   └── traccarhome
│       ├── conf
│       └── logs
├── project
│   ├── lighthouse
│   │   ├── common
│   │   ├── dispatcher
│   │   ├── pg_watcher
│   │   └── positions
│   ├── old
│   └── test
└── python
```

# 구성
## Docker 에 관하여
jaunt-lighthouse 는 python3 로 작성되었으며, [scikit-mobility](https://scikit-mobility.readthedocs.io/en/latest/) 를 사용하여 위치 데이터를 분석하고 있습니다. 따라서 해당 패키지를 설치해야 합니다.

해당 패키지의 종속성 문제를 방지하고 배포를 용이하기 위해 Docker 로 배포됩니다.

## Docker 로 실행하기 - docker-compose

이 프로젝트에는 현재 2 개의 docker-compose.yml 이 제공됩니다. 각 파일을 참조하여 상황에 맞게 서비스를 구동해주세요.

### python 환경 (./docker-compose.yml)
Traccar 를 직접 실행하는 데 필요한 서비스를 __제외한__ 모든 서비스들을 포함합니다.

- python: python3
- pgadmin: postgresql query 도구
- mongo: mongodb 서버. python 실행 결과가 저장됩니다.
- mongoexpress: mognodb query 도구
- redis: redis 서버. python 실행 중 임시 저장소로 사용됩니다.

적절한 설정을 위해 .env.sample 파일을 참조하여 .env 파일을 생성하세요.

### 테스트용 Traccar 서버 (./dev.server/docker-compose.yml)
테스트용 Traccar 를 실행하는 서비스들입니다.

- traccar: traccar docker image. 5055 포트로 GPS 데이터를 수신하고 (osmand), 8082 포트로 웹 서버를 실행합니다.
- postgres: postgres docker image. 위치데이터를 포함, Traccar 데이터가 모두 저장됩니다. docker 로 실행 시 저장되는 모든 시간대는 UTC 입니다.

## 직접 구성하기
Docker 를 사용하지 않는다면 다음과 같은 구성을 직접 해야 합니다.

- Scikit-mobility 가 설치된 Python3 환경
- Traccar 
- Postgresql: Traccar 서버가 사용하고 있어야 하고 위의 python3 환경에서 접속할 수 있어야합니다.

# 실행
## 테스트 서버 실행

실행 전 설정파일 수정
- dev.server 의 .env 파일은 docker-compose.yml 에 적용됩니다. 키=값 형태로 작성합니다.
- dev.server/traccarhome/conf/traccar.xml 은 traccar 서비스가 시작되면서 사용됩니다. [Traccar Configuration File](https://www.traccar.org/configuration-file/) 를 참조하여 xml 형식의 파일을 작성하세요. 기본적으로 제공되는 DB 연결 URL 만 올바르게 작성해도 작동합니다.

```
# traccar.xml
<entry key='database.url'>jdbc:postgresql://{실행 중인 postgresql 의 IP 또는 도메인}/{데이터베이스명}</entry>
<entry key='database.user'>{DB사용자}</entry>
<entry key='database.password'>{DB비밀번호}</entry>
```

```
# 실행
cd dev.server
docker-compose up -d

# 종료
cd dev.server
docker-compose down -v
```

설정값이 올바르지 않다면 traccar 가 계속 재시작됩니다. 종료하고 설정파일을 수정한 뒤 재실행하세요.

## Python 실행

### Docker 이용

Python 환경 실행
```
# 실행
docker-compose up -d

# 종료
docker-compose down -v
```

Python Container 내에서 App 실행

```
# Container 접속
docker exec -it jaunt-lighthouse_python_1 bash

# Container 안에서
cd /workspace/project/lighthouse
python app.py
```

### 직접 설치된 Python 환경
```
cd /workspace/project/lighthouse
python app.py
```

## 모듈 설명
```
lighthouse
├── app.py
├── common
│   ├── config.py
│   ├── daemon_thread.py
│   ├── mongo.py
│   ├── postgres.py
│   └── traccar_connection.py
├── config.yml
├── dispatcher
├── pg_watcher
│   └── pg_trigger.py
├── positions
│   ├── positions.py
│   ├── store.py
│   └── traccar_models.py
├── requirements.txt
├── tc_position_handler.py
├── test-generator.py
└── test-replay.py
```
### 메인 설정 파일 (config.yml)

실행을 위한 모든 설정파일이 포함됩니다. config.yml.sample 을 참고하여 작성하세요.


### 메인 App (app.py)

두가지 핵심 메커니즘이 존재합니다.

#### pg_trigger

Postgresql DB Table 에 Trigger 를 생성하고 변화를 관찰합니다.
현재 코드에서는 Insert 만 감지하고 있으나 필요한 경우 복잡한 SQL 트리거를 만들 수 있습니다.

Insert 되었을 때의 행동을 for 문 안에서 정의합니다.

### Daemon Thread

메인 스레드와 별개로 실행됩니다.

메인 스레드가 잘 실행되고 있는지를 위해 사용됩니다.

run_thread() 메서드에 첫번째 인자로 실행할 함수를 전달합니다.


### Stay point 검출 모듈 (positions.py)

Trigger 로 인해 새로운 Insert 가 발생할 때마다 실행됩니다.

detect_new_stop(data):
    Table 에 새롭게 추가된 행 정보 (data) == new_position

1. new_position 에서 deviceid 를 추출
1. 해당 deviceid 의 기준점 위치(f_pid)를 찾는다.
   - 없으면 new_position.id 가 f_pid 가 되고 종료.
1. f_pid 부터 new.position.id 까지의 점들에서 stay point 를 찾는다.
   1. Stay point 가 발견되지 않을 경우: 이동 중으로 생각하고 종료.
   1. Stay point 가 하나 발견될 경우: Stay point 와 new_position 이 Stay point 검출 기준보다 멀 경우 검출 저장하고 종료.
   1. Stay point 가 두개 발견될 경우: 원칙적으로 실시간 모니터링에서는 발생할 수 없으나 데이터가 뭉쳐서 전달되는 경우 발생할 수 있음. 마지막 Stay point 을 대기시키고 종료.


### test-generator.py

지정된 Waypoint 를 따라 조금씩 움직이는 GPS 지점을 생성하고 해당 지점을 traccar 서버에 전달합니다.

### test-replay.py

지정된 파일(csv)로부터 GPS 위치 정보를 읽어서 traccar 서버에 전송하거나 DB 에 직접 추가합니다.

traccar 서버에 전송 주기가 너무 짧으면 위치 데이터의 순서가 뒤바뀔 수 있습니다.