import skmob
from skmob.preprocessing import filtering, compression, detection
import numpy as np
import pandas as pd
from time import time
from position import append_position, detect_new_stop_with

tdf = skmob.TrajDataFrame.from_file("geolife_sample.txt.gz").sort_values(by="datetime")

# user 하나로만 실험
user_tdf = tdf[tdf.uid == 1]
user_ftdf = filtering.filter(user_tdf, max_speed_kmh=500.0)
user_ctdf = compression.compress(user_ftdf, spatial_radius_km=0.1)


def create_fake_data(lat, lng, datetime, uid):
    device_id = uid
    elem_tdf = {"lat": lat, "long": lng, "datetime": datetime, "uid": uid}
    return [device_id, elem_tdf]


result = [
    create_fake_data(lat, lng, datetime, uid)
    for lat, lng, datetime, uid in zip(user_ctdf["lat"], user_ctdf["lng"], user_ctdf["datetime"], user_ctdf["uid"])
]
# print(result[0])
# print(len(result))

device_stack = {}
device_list = [1, 2, 3]  # 추후 db에서 불러올 값
stops = []

for device in device_list:
    device_stack[device] = []  # 초기화

# stop detection 시작
for i in range(len(result)):
    # print(device_stack[1])
    device_id = 1
    elem_tdf = result[i][1]
    device_stack[device_id] = append_position(device_stack[device_id], elem_tdf)
    new_stop = detect_new_stop_with(device_stack[device_id])
    # print("new stop: ", new_stop)

    if len(new_stop) > 0:
        stops.append(new_stop)
        device_stack[device_id] = []

print(stops)
# print("# of stops:", len(stops))
