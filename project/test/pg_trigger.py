from position import detect_new_stop_with, append_position
import psycopg2
import select
import requests
import traceback
from datetime import datetime
from typing import List, Tuple
from config import dry_run

channel_name = "channel"


def create_trigger(conn, trg_name: str, table: str):
    sql_create_fn = f"""CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
  DECLARE
    record RECORD;
    payload JSON;
  BEGIN
    IF (TG_OP = 'DELETE') THEN
      record = OLD;
    ELSE
      record = NEW;
    END IF;
    payload = json_build_object('table', TG_TABLE_NAME,
                                'action', TG_OP,
                                'data', row_to_json(record));
    PERFORM pg_notify('{channel_name}', payload::text);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;"""
    sql_trigger = """CREATE TRIGGER {}
  AFTER INSERT ON {}
  FOR EACH ROW EXECUTE PROCEDURE notify_event();""".format(
        trg_name, table
    )

    curs = conn.cursor()
    curs.execute(sql_create_fn)
    curs.execute(sql_trigger)
    print("execute listening")
    conn.commit()


def status_triggers(conn) -> List[Tuple[str, str]]:
    curs = conn.cursor()
    curs.execute("SELECT * FROM information_schema.triggers")
    results = curs.fetchall()
    triggers = list(map(lambda row: (row[2], row[6]), results))  # trigger name & table name
    return triggers


def listen_trigger(conn, trg_name: str, table: str, timeout: int = 5):
    # TODO: 테스트 모드 (dry run) 만들기. Dry run 일 경우 stop 검출은 하지만 관리 서버 (현재 db.hanyang.ac.kr/jaunt) 에 http request는 하지 않음
    # endpoint 명: update/stop
    if True not in [(tup[0] == trg_name and tup[1] == table) for tup in status_triggers(conn)]:
        create_trigger(conn, trg_name, table)

    curs = conn.cursor()
    curs.execute(f"LISTEN {channel_name};")

    device_stack = []
    device_list = [1, 2, 3]  # 추후 db에서 불러올 값

    for device in device_list:
        device_stack[device] = []  # 초기화

    while True:
        if select.select([conn], [], [], timeout) == ([], [], []):
            print("TIMEOUT")
        else:
            conn.poll()
            while conn.notifies:
                notify = conn.notifies.pop(0)
                print(datetime.now().isoformat())
                print("Got NOTIFY:", notify.pid, notify.channel, notify.payload)

                # TODO: GET NEW Stops through dataframe
                device_id = data["deviceid"]
                data = notify.payload["data"]
                stack = append_position(device_stack[device_id], data)  # stack of tdf(per device)
                new_stop = detect_new_stop_with(stack)
                if len(new_stop) > 0:
                    device_stack[device_id] = []  # empty stack

                if not dry_run and new_stop:
                    try:
                        headers = {"Content-Type": "application/json"}
                        r = requests.put(
                            "http://db.hanyang.ac.kr:80/jaunt/update/stop", headers=headers, data=notify.payload
                        )
                        if r.status_code == 200:
                            print("MSG SENT", r.status_code)
                        else:
                            print("Request FAILED", r.status_code)
                    except Exception:
                        traceback.print_exc()
                        print("Server no response")


def drop_trigger(conn, trg_name: str, table: str):
    sql_drop = """DROP TRIGGER IF EXISTS {} ON {};""".format(trg_name, table)
    curs = conn.cursor()
    curs.execute(sql_drop)
    print("trigger dropped")
    conn.commit()


def main():
    connectin_url = ""
    assert connectin_url, "Postgres Server Info needed"
    try:
        conn = psycopg2.connect(connectin_url)
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        conn.commit()

    except psycopg2.Error as e:
        print(e)
        exit()

    try:
        create_trigger(conn, "notify_event", "tc_positions", 5)
    except Exception:
        traceback.print_exc()
        print("unable to create a trigger")


if __name__ == "__main__":
    print("Run", datetime.now().isoformat())
    main()
