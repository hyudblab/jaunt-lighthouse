from skmob import TrajDataFrame
from skmob.preprocessing import detection
import pandas as pd

# check datatype of elem of elem_tdf
def stack_up(stack, elem_tdf):
    # [INFO] 서버타임은 fixtime 또는 device 타임 기준으로 하되 UTC 기준임
    
    if len(stack) == 0:
        df = pd.DataFrame(elem_tdf, index=[0])
        stack = TrajDataFrame(df, latitude="lat", longitude="long", user_id="uid", datetime="datetime")
        # print("stack2: ",stack)
        return stack

    else:
        df = pd.DataFrame(elem_tdf, index=[0])
        tdf = TrajDataFrame(df, latitude="lat", longitude="long", user_id="uid", datetime="datetime")
        stack = stack.append(tdf)
        return stack

def detect_new_stop_with(stack):
    stdf = detection.stops(stack)
    # print("stdf: ", stdf)

    if len(stdf) == 0:
        return []

    else:
        tmp_li = [stack, stdf.iloc[0].to_dict()]
        # mongodb["stops"].insert_one(tmp_li)
        return stdf.iloc[-1:]