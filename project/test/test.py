import skmob
from skmob.preprocessing import filtering, compression, detection
import numpy as np
import pandas as pd
from time import time

tdf = skmob.TrajDataFrame.from_file("geolife_sample.txt.gz").sort_values(by="datetime")

# user 하나로만 실험
# user_tdf = tdf[tdf.uid == 1]
# user_ftdf = filtering.filter(user_tdf, max_speed_kmh=500.)
# user_ctdf = compression.compress(user_ftdf, spatial_radius_km=0.1)
# user_ctdf['idx'] = np.random.randint(1, 8000, user_ctdf.shape[0])


# 여러 유저로 실험(3명)
user_tdf = tdf[tdf.uid == 1]
np.random.seed(0)
rand_uid = np.random.randint(1, 4, user_tdf.shape[0])  # uid 조작
rand_idx = np.random.randint(1, 110000, user_tdf.shape[0])
user_tdf = user_tdf.copy()
user_tdf.loc[:, "uid"] = rand_uid
user_tdf.loc[:, "idx"] = rand_idx
user_ctdf = user_tdf
print(user_ctdf.head())


def create_fake_data(lat, lng, datetime, uid, idx):
    device_id = uid
    elem_tdf = {"lat": lat, "long": lng, "datetime": datetime, "uid": uid}
    idx = idx
    return [device_id, elem_tdf, idx]


result = [
    create_fake_data(lat, lng, datetime, uid, idx)
    for lat, lng, datetime, uid, idx in zip(
        user_ctdf["lat"], user_ctdf["lng"], user_ctdf["datetime"], user_ctdf["uid"], user_ctdf["idx"]
    )
]
# print(result[0])
# print(len(result))

df_stack = {}  # dict of dataframes. key는 device_id, value는 trajectory dataframe
device_list = []
window_per_device = {}
stops = []

# stop detection 시작
# for i in range(len(result)):
for i in range(1000):
    device_id = result[i][0]
    elem_tdf = result[i][1]
    if device_id not in device_list:
        device_list.append(device_id)
        df_stack[device_id] = pd.DataFrame(elem_tdf, index=[0])
        window_per_device[device_id] = []

    else:
        df_stack[device_id] = df_stack[device_id].append(elem_tdf, ignore_index=True)

    start_time = time()
    tdf = skmob.TrajDataFrame(df_stack[device_id], latitude="lat", longitude="long", user_id="uid", datetime="datetime")
    tdf.parameters["id"] = result[i][2]

    stdf = detection.stops(tdf)
    # print("stdf: ", stdf)
    end_time = time()
    print("time consumed: ", end_time - start_time)

    if len(stdf) == 0:
        window_per_device[device_id].append(result[i][2])
        # print('when stdf==0: ', window_per_device)

    else:
        # start_point = window_per_device[device_id][0]
        # end_point = window_per_device[device_id][-1]
        tmp_li = [device_id, window_per_device[device_id], stdf.iloc[0].to_dict()]
        stops.append(tmp_li)  # test가 아니라면 mongo에 저장

        # print('flushing')
        df_stack[device_id] = df_stack[device_id].iloc[-1:]  # tdf 비우고 다시 시작
        window_per_device[device_id] = []
        window_per_device[device_id].append(result[i][2])

print(stops)
print("# of stops:", len(stops))
