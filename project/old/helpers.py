from datetime import datetime
from typing import Union
from dateutil import tz
from dateutil.parser import parse


def parse_utc_date(date: Union[datetime, str], dest_tz="Asia/Seoul") -> datetime:
    return parse(date).astimezone(tz.gettz(dest_tz)) if isinstance(date, str) else date.astimezone(tz.gettz(dest_tz))


def parse_utc_date_as_formatted(date, dest_tz="Asia/Seoul", format="%m월 %d일, %H:%M") -> str:
    date = parse_utc_date(date, dest_tz)
    return date.strftime("%m월 %d일, %H:%M".encode("unicode-escape").decode()).encode().decode("unicode-escape")


def parse_to_utc(date: Union[datetime, str], source_tz="Asia/Seoul") -> datetime:
    return (
        parse(date).replace(tzinfo=tz.gettz(source_tz)).astimezone(tz.gettz("UTC"))
        if isinstance(date, str)
        else date.replace(tzinfo=tz.gettz(source_tz)).astimezone(tz.gettz("UTC"))
    )
