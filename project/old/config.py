from typing import Callable, Any
import os
import json

config_mode = os.getenv("RUNTIME_ENV", "development")


def read_properties(setting, filename="db.properties"):
    """Get secret setting or return None"""
    with open(filename) as f:
        secrets = json.load(f)
        return secrets.get(setting)


def config_properties(env: str) -> Callable[[str], Any]:
    filename = "db"
    if env is not None:
        filename += "." + env
    filename += "." + "properties"
    print("CONFIG FILE", filename)
    return lambda name: read_properties(name, filename)


get_config = config_properties(config_mode)
