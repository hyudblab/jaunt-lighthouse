from typing import Any
from PositionData import TcPositionDataFrame
from helpers import parse_to_utc
from config import get_config
from TraccarConnection import TraccarConnection
from pprint import pprint
from position import detect_new_stop_with
from pymongo import MongoClient
import json
from dateutil import tz
import requests
import traceback


def send_message(url: str, data: Any):
    url = "http://host.docker.internal:54440/report/update"
    try:
        headers = {"Content-Type": "application/json"}
        r = requests.put(url, headers=headers, data=data)
        if r.status_code == 200:
            print("MSG SENT", r.status_code)
        else:
            print("Request FAILED", r.status_code)
    except Exception:
        traceback.print_exc()
        print("Server no response")


def main():
    traccar = TraccarConnection(get_config("traccar"))
    client = MongoClient(
        "{drivername}://{username}:{password}@{host}:{port}/{database}?authSource={authDb}".format(
            **get_config("mongo")
        )
    )
    deviceid = 3
    device_collection = client["traccarStop"]["device"]
    device_collection.delete_one({"deviceid": deviceid})

    date_from = parse_to_utc("2020-12-02T09:30").isoformat()
    date_to = parse_to_utc("2020-12-02T18:02:30").isoformat()
    positions = traccar.GET("positions", {"deviceId": deviceid, "from": date_from, "to": date_to})

    pprint(len(positions))
    pprint(positions[0])

    for item in positions:
        data = dict((k.lower(), v) for k, v in item.items())
        del data["type"]
        del data["outdated"]
        data["devicetime"] = data["devicetime"][:-5]
        payload = json.dumps({"data": data})
        result = detect_new_stop_with(payload, client)
        if not result.empty:
            print("Stop updated")
            print(result)
            stop = result.iloc[0]
            print(type(stop.to_json()), stop.to_json())
            send_message("", stop.to_json())
            break
            # print(result.head())
            # dt = result.iloc[0]["leaving_datetime"].tz_localize(tz.gettz("utc"))
            # tz_info = tz.gettz("Asia/Seoul")
            # dt = dt.to_pydatetime().astimezone(tz_info).strftime("%m/%d/%Y, %H:%M:%S")
    # send_message("", result.to_json())


def summary():
    client = MongoClient("{drivername}://{username}:{password}@{host}:{port}/".format(**get_config("mongo")))
    deviceid = 3
    device_collection = client["traccarStop"]["device"]
    device = device_collection.find_one({"deviceid": deviceid})
    df = TcPositionDataFrame.from_list_orient_dict(device["positions"])
    print(df.max())
    print(df.min())
    print(df.mean())
    print(df.std())


if __name__ == "__main__":
    main()
    # summary()
