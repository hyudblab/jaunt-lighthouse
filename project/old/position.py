import dateutil.parser
from typing import Any, Optional, Union
import json
from pymongo import MongoClient
from pymongo.collection import Collection as MongoCollection
import pandas as pd
from skmob import TrajDataFrame
from skmob.preprocessing import filtering, detection
from PositionData import TcPosition, TcPositionDataFrame


class DeviceMongoAdapter:
    deviceid: int
    device: Any
    tdf: Union[TcPositionDataFrame, TrajDataFrame]
    stdf: Optional[TrajDataFrame] = None
    collection: MongoCollection

    def __init__(self, collection: MongoCollection, deviceid: int) -> None:
        self.collection = collection
        self.deviceid = deviceid
        self.device = collection.find_one({"deviceid": deviceid})

    def set_first_positions(self, position: TcPosition):
        self.tdf = TcPositionDataFrame.init_df(position)
        self.collection.insert_one({"deviceid": self.deviceid, "positions": self.positions})

    @property
    def positions(self):
        return self.tdf.to_dict("list")

    @positions.setter
    def positions(self, a_positions):
        self.tdf = TcPositionDataFrame.from_list_orient_dict(a_positions)

    @property
    def stops(self):
        return self.stdf.to_dict("list")

    @stops.setter
    def stops(self, a_stops):
        self.stdf = TcPositionDataFrame.from_list_orient_dict(a_stops) if a_stops else None

    def read(self):
        self.positions = self.device.get("positions")
        self.stops = self.device.get("stops")
        return self.tdf, self.stdf

    def update(self):
        if self.stdf is None:
            self.collection.update_one({"deviceid": self.deviceid}, {"$set": {"positions": self.positions}})
        else:
            self.collection.update_one(
                {"deviceid": self.deviceid}, {"$set": {"positions": self.positions, "stops": self.stops}}
            )


def filter_and_detect_stops(tdf: TrajDataFrame) -> TrajDataFrame:
    ftdf = filtering.filter(tdf, max_speed_kmh=300.0, include_loops=True)
    stdf = detection.stops(ftdf)
    return stdf


def detect_new_stop_with(payload: str, client: MongoClient) -> Union[TrajDataFrame, pd.DataFrame]:
    data = json.loads(payload)["data"]
    position = TcPosition(**data)
    deviceid = position.deviceid
    devices = client["traccarStop"]["device"]
    device_adapter = DeviceMongoAdapter(devices, deviceid)  # 별로 좋은 이름은 아닌데 생각이 안난다

    if device_adapter.device is None:
        device_adapter.set_first_positions(position)
    else:
        last_tdf, _ = device_adapter.read()
        tdf = last_tdf.append_position(position, filtering=True)
        stdf = filter_and_detect_stops(tdf)

        if len(stdf) == 0:
            device_adapter.tdf = tdf
            device_adapter.update()
        else:
            if is_any_new_stop(position, stdf):
                device_adapter.tdf = TcPositionDataFrame.init_df(position)
                device_adapter.stdf = stdf
                device_adapter.update()
                print("STOP FLUSHING")
                print(stdf.head())
                return stdf
            else:
                device_adapter.tdf = tdf
                device_adapter.stdf = stdf
                device_adapter.update()
    return pd.DataFrame()


def is_any_new_stop(position: TcPosition, stop_df: TrajDataFrame) -> bool:
    last_stop = stop_df.iloc[-1]
    timedelta = dateutil.parser.parse(position.devicetime) - last_stop["leaving_datetime"]
    total = timedelta.total_seconds()
    if total > 60 * 15:
        return True
    return False
