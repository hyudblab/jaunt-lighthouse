from typing import Callable, List, Optional, Tuple, Any
import psycopg2
import select
import traceback


class PgTrigger:
    def __init__(
        self,
        name: str,
        table: str,
        channel: str = "channel",
        timeout: int = 5,
        config_callback: Optional[Callable[[str], Any]] = None,
        conn: Optional[Any] = None,
    ):
        print("CREATE PG_TRIGGER")
        self.name = name
        self.table = table
        self.channel = channel
        self.timeout = timeout

        # Configuration File has priority than conn or dry_run values
        if config_callback:
            pg_config = config_callback("postgres")
            print("CONNECTING TO Postgres", pg_config)
            pg_url = "{drivername}://{username}:{password}@{host}:{port}/{database}".format(**pg_config)
            assert pg_url, "Postgres Server Info needed"
            self.conn = psycopg2.connect(pg_url)
            self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            print("CONNECTED TO Postgres")
        else:
            if conn:
                self.conn = conn
            else:
                print("Trigger Created with non Connection! Must set up later")

    @staticmethod
    def status(conn) -> List[Tuple[str, str]]:
        try:
            curs = conn.cursor()
            curs.execute("SELECT * FROM information_schema.triggers")
            results = curs.fetchall()
            trigger_index = 2
            table_index = 6
            triggers = list(map(lambda row: (row[trigger_index], row[table_index]), results))
            return triggers
        except Exception:
            print("[STATUS] Cannot Connect DB")
            traceback.print_exc()
            raise

    def create(self) -> bool:
        sql_create_fn = f"""
        CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
          DECLARE
            record RECORD;
            payload JSON;
            BEGIN
                IF (TG_OP = 'DELETE') THEN
                record = OLD;
                ELSE
                record = NEW;
                END IF;
                payload = json_build_object('table', TG_TABLE_NAME,
                                            'action', TG_OP,
                                            'data', row_to_json(record));
                PERFORM pg_notify('{self.channel}', payload::text);
                RETURN NULL;
            END;
        $$ LANGUAGE plpgsql;
        """
        sql_trigger = """CREATE TRIGGER {} AFTER INSERT ON {} FOR EACH ROW EXECUTE PROCEDURE notify_event();""".format(
            self.name, self.table
        )
        try:
            curs = self.conn.cursor()
            curs.execute(sql_create_fn)
            print(f"CREATE FUNCTION on CHANNEL {self.channel}")
            curs.execute(sql_trigger)
            print(f"CREATE TRIGGER {self.name} on {self.table}")
            self.conn.commit()
            return True
        except Exception:
            print("[CREATE TRIGGER] Failed")
            traceback.print_exc()
            raise

    def drop(self) -> None:
        sql_drop = """DROP TRIGGER IF EXISTS {} ON {};""".format(self.name, self.table)
        curs = self.conn.cursor()
        curs.execute(sql_drop)
        print(f"DROP TRIGGER {self.name} on {self.table}")
        self.conn.commit()

    def __iter__(self):
        current_triggers = [(tup[0] == self.name and tup[1] == self.table) for tup in PgTrigger.status(self.conn)]
        if True not in current_triggers:
            self.create()

        curs = self.conn.cursor()
        curs.execute(f"LISTEN {self.channel}")
        print(f"LISTEN {self.channel}")
        return self

    def __next__(self) -> str:
        while True:
            if not (select.select([self.conn], [], [], self.timeout) == ([], [], [])):
                self.conn.poll()
                while self.conn.notifies:
                    notify = self.conn.notifies.pop(0)
                    return notify.payload
