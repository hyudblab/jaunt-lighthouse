import argparse
from typing import Any
from pymongo.mongo_client import MongoClient
from config import get_config
from pg_trigger import PgTrigger
from position import detect_new_stop_with
import requests
import traceback


def main(args):
    print("RUN pg_trigger")
    trigger = PgTrigger("tc_positions_trigger", "tc_positions", config_callback=get_config)
    mongo_url = "{drivername}://{username}:{password}@{host}:{port}".format(**get_config("mongo"))
    client = MongoClient(mongo_url)
    print("MONGO CONNECTION", client.server_info())
    command = args.command
    url = get_config("update_url")
    print("Stops will be sent to", url)
    if command == "create":
        trigger.create()
    elif command == "status":
        print(PgTrigger.status(trigger.conn))
    elif command == "drop":
        trigger.drop()
    else:
        for payload in trigger:
            result = detect_new_stop_with(payload, client)
            if not get_config("dry_run") and not result.empty and url:  # URL Validation needed
                send_message(url, result.iloc[0].to_json())
    client.close()


def send_message(url: str, data: Any):
    print("SENDING DATA", url, data)
    try:
        headers = {"Content-Type": "application/json"}
        r = requests.put(url, headers=headers, data=data)
        if r.status_code == 200:
            print("MSG SENT", r.status_code)
        else:
            print("Request FAILED", r.status_code)
    except Exception:
        traceback.print_exc()
        print("Server no response")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command", nargs="?", help="Trigger Command: create, status, or drop")
    args = parser.parse_args()
    main(args)
