from common.daemon_thread import prompt_in_terminal, run_thread
from common.config import read_config
from pg_watcher.pg_trigger import PgTrigger
import json
from positions.positions import detect_new_stop


def run_trigger(trigger: PgTrigger):
    print("[TRIGGER] Reading Status")

    running_triggers = PgTrigger.status(trigger.conn)
    print(running_triggers)

    if len(running_triggers) > 0:
        # TODO: If we have two or more triggers, we must check existing triggers
        trigger.drop()

    print("[TRIGGER] Create Trigger")
    trigger.create()

    print("[TRIGGER] Waiting payloads...")
    for payload in trigger:
        data = json.loads(payload)["data"]
        yield data


def main():
    # Read configuration
    configs = read_config()

    # Create Trigger
    pg_trigger = PgTrigger(pg_config=configs["postgres"])

    # Start background thread
    heartbeat_timeout = 5
    run_thread(prompt_in_terminal, heartbeat_timeout)

    # Start trigger
    for new_position in run_trigger(pg_trigger):
        # Do something with new payload
        detect_new_stop(new_position)


if __name__ == "__main__":
    main()
