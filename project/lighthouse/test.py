from common.mongo import get_device, add_stay_location
from positions.positions import distance_from_stay_location, filter_and_detect_stops
from positions.store import get_first_position_after, get_positions_df_by_uniqueId, get_uniqueId_from_deviceid


def read_current_positions(uniqueId: str):
    df = get_positions_df_by_uniqueId(uniqueId, 0)
    return df


def get_last_position(uniqueId: str):
    df = read_current_positions(uniqueId)
    return df.iloc[-1]


def test_stay_location_update():
    pass


def main():
    uniqueId = "user_4332"
    df = read_current_positions(uniqueId)
    stdf = filter_and_detect_stops(df)
    last_location = get_last_position(uniqueId)
    print(last_location)
    distance = distance_from_stay_location(stdf, last_location.latitude, last_location.longitude)
    interval = (last_location.fixtime.to_pydatetime() - stdf.iloc[0].leaving_datetime.to_pydatetime()).total_seconds()
    print("gap", last_location.fixtime.to_pydatetime(), stdf.iloc[0].leaving_datetime.to_pydatetime())
    print("Distance from last stop (km):", distance, "Interval (minute):", interval / 60)
    print(stdf.head().to_markdown())

    deviceid = int(stdf.iloc[0]["uid"])
    mg_device = get_device(deviceid)
    mg_device = deviceid if mg_device is None else mg_device
    print("비어있겠지", mg_device)
    print(stdf.to_dict())
    uniqueId = get_uniqueId_from_deviceid(mg_device)

    add_stay_location(stdf.iloc[0], uniqueId)

    last_stay = stdf.iloc[0]
    last_datetime = last_stay.leaving_datetime.to_pydatetime()
    new_pid = get_first_position_after(deviceid, last_datetime)
    print("New position id:", new_pid)


main()
