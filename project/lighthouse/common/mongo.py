from datetime import datetime
from typing import Any, List
from pymongo.mongo_client import MongoClient
from skmob import TrajDataFrame
from common.config import read_config


# Mongo connections
configs = read_config()
mongo_url = "mongodb://{username}:{password}@{host}:{port}".format(**configs["mongo"])
print(mongo_url)
client = MongoClient(mongo_url)


def get_device(deviceid: int):
    """
    Read the device from the Mongo database.
    """
    db = client.traccar
    return db.devices.find_one({"id": deviceid})


def add_stay_location(location_df: TrajDataFrame, uniqueId: str, tag: str = None):
    """
    Add the stay location of a device.
    """

    data_columns = ["id", "uid", "datetime", "lat", "lng", "altitude", "leaving_datetime"]
    data = location_df[data_columns].to_dict()
    if uniqueId:
        data["uniqueId"] = uniqueId

    if tag:
        data["tag"] = tag

    data["deviceid"] = data["uid"]
    data["raw"] = location_df.to_json()

    db = client.traccar
    db.devices.insert_one(data)


def get_stay_locations(deviceid: int, start: datetime = None, end: datetime = None) -> List[Any]:
    """
    Read the stay locations of a device.
    """

    db = client.traccar
    start = start if start else datetime(1970, 1, 1)
    if end is None:
        return db.devices.find({"deviceid": deviceid, "datetime": {"$gte": start}})
    else:
        return db.devices.find({"deviceid": deviceid, "datetime": {"$gte": start, "$lte": end}})


def get_devices() -> List[int]:
    """
    Get the deviceids from the Mongo database.
    """
    db = client.traccar
    return db.devices.distinct("deviceid")
