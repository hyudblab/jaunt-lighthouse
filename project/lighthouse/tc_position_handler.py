import yaml
from common.postgres import engine_sqlalchemy
import pandas as pd


with open("config.yml") as f:
    configs = yaml.load(f, Loader=yaml.FullLoader)

# engine = engine_sqlalchemy(**configs["postgres-aws"])
engine = engine_sqlalchemy(**configs["postgres-2018"])

tc_positions_columns = [
    "id" "protocol",
    "deviceid",
    "servertime",
    "devicetime",
    "fixtime",
    "valid",
    "latitude",
    "longitude",
    "altitude",
    "speed",
    "course",
    "address",
    "attributes",
    "accuracy",
    "network",
]


def db_to_csv(timezone: str = None, uniqueId=True):
    query = ""
    select_query = "tp.*"
    if uniqueId:
        query = "select tp.*, td.uniqueid from tc_positions tp, tc_devices td where td.id = tp.deviceid order by tp.id"
    else:
        query = ""
    df = pd.read_sql(query, engine)
    print(df.head())
    df.to_csv("tc_positions_tester_aws.csv", index=False)


def check_csv():
    df = pd.read_csv("tc_positions_tester_aws.csv")
    print(df.head())


def csv_to_db():
    df = pd.read_csv("tc_positions_tester_aws.csv")
    df.to_sql("tc_positions", engine, if_exists="replace", index=False)


def append_to_db(start=0, end=-1):
    df = pd.read_csv("tc_positions_tester_aws.csv")
    df[start:end].to_sql("tc_positions", engine, if_exists="append", index=False)


# def clear_table():
#     conn = engine.raw_connection()
#     pass


def main():
    # db_to_csv()
    # check_csv()
    # csv_to_db()
    # append_to_db(0, 10)
    q = """
        SELECT
            tp.id, tp.deviceid, tp.fixtime at time zone 'Asia/Seoul' at time zone 'utc' as datetime,
           tp.latitude, tp.longitude, tp.speed, tp.course, tp.accuracy
          FROM tc_positions tp, tc_devices td
          WHERE
            td.name not like ('#%%') and td.name not like ('@%%') and lower(td.name) not like ('%%test%%') and td.name != '01' and td.name != '02'
            AND td.id = tp.deviceid
          ORDER BY tp.id"""
    df = pd.read_sql(q, engine)
    df.to_csv("tc_positions_2018_wheelchair_users.csv", index=False)


if __name__ == "__main__":
    main()
