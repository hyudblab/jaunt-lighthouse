from typing import Union
from dateutil.parser import isoparse
from urllib.parse import urlencode
import time
import http.client
import pandas as pd
from pandas._libs.tslibs.timestamps import Timestamp
from common.config import read_config
from common.traccar_connection import TraccarConnection
from common.postgres import connect_psycopg2
from datetime import datetime


server = "host.docker.internal:5055"
conn = http.client.HTTPConnection(server)
configs = read_config()
pg_conn = connect_psycopg2(**configs["postgres"])
traccar_conn = TraccarConnection(configs["traccar"])


def create_osmand_requests(
    uniqueId: str,
    lat: float,
    lng: float,
    timestamp: Union[Timestamp, str, int],
    altitude: float = None,
    speed: float = 0,
):
    if type(timestamp) == str:
        timestamp = isoparse(timestamp).timestamp()
    elif isinstance(timestamp, Timestamp):
        timestamp = timestamp.to_pydatetime().timestamp()
    params = {
        "id": uniqueId,
        "lat": lat,
        "lon": lng,
        "timestamp": int(timestamp),
        "altitude": altitude,
        "speed": speed,
    }

    print(urlencode(params))
    conn.request("GET", "?" + urlencode(params))
    conn.getresponse().read()


def replay_trajectory(df, period=1):
    for index, row in df.iterrows():
        create_osmand_requests(
            uniqueId=row["uniqueId"],
            lat=row["latitude"],
            lng=row["longitude"],
            timestamp=row["fixtime"],
            altitude=row["altitude"],
            speed=row["speed"],
        )
        time.sleep(period)


def replay_trajectory_in_pg(df, period=1):
    for index, row in df.iterrows():
        with pg_conn.cursor() as cur:
            query = """
            insert into tc_positions
              (protocol, deviceid, servertime, devicetime, fixtime, valid,
               latitude, longitude, altitude, speed, course, accuracy)
            values
              ('replay', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """
            cur.execute(
                query,
                (
                    row["deviceid"],
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    row["devicetime"],
                    row["fixtime"],
                    True,
                    row["latitude"],
                    row["longitude"],
                    row["altitude"],
                    row["speed"],
                    row["course"],
                    row["accuracy"],
                ),
            )
        time.sleep(period)


def create_user_from_deviceid(deviceid: str) -> str:
    uniqueId = f"user_{deviceid}"
    devices = traccar_conn.GET("/devices")
    unique_ids = [device["uniqueId"] for device in devices]
    if uniqueId not in unique_ids:
        traccar_conn.POST("/devices", {"uniqueId": uniqueId, "name": uniqueId})
    new_device_id = traccar_conn.GET(f"/devices?uniqueId={uniqueId}")[0]["id"]
    return uniqueId, new_device_id


def create_user_from_uniqueId(uniqueId: str) -> str:
    devices = traccar_conn.GET("/devices")
    unique_ids = [device["uniqueId"] for device in devices]
    if uniqueId not in unique_ids:
        traccar_conn.POST("/devices", {"uniqueId": uniqueId, "name": uniqueId})
    new_device_id = traccar_conn.GET(f"/devices?uniqueId={uniqueId}")[0]["id"]
    return uniqueId, new_device_id


def create_replay_df_from_csv(csv_path: str) -> pd.DataFrame:
    df = pd.read_csv(csv_path, low_memory=False)
    df["fixtime"] = df["fixtime"].apply(lambda x: isoparse(x))
    return df


def delete_replay_users():
    devices = traccar_conn.GET("/devices")
    for device in devices:
        uniqueId = device["uniqueId"]
        deviceid = device["id"]
        if uniqueId.startswith("user_"):
            print("Deleting user", uniqueId, deviceid)
            traccar_conn.DELETE(f"/devices/{deviceid}")


def delete_all_users():
    devices = traccar_conn.GET("/devices")
    for device in devices:
        deviceid = device["id"]
        traccar_conn.DELETE(f"/devices/{deviceid}")


def main():
    # Delete users before replaying
    delete_all_users()
    # delete_replay_users()

    # Load replay data
    csv_path = "tc_positions_tester_aws.csv"
    df = create_replay_df_from_csv(csv_path)

    # Remove Positions outside Korea penninsula
    korean_east = 131.5222
    korean_west = 124.3636
    korean_north = 38.3640
    korean_south = 33.0643
    df = df[(df["latitude"] < korean_north) & (df["latitude"] > korean_south)]
    df = df[(df["longitude"] < korean_east) & (df["longitude"] > korean_west)]
    print(df.head().to_markdown())

    # Assign new device id to each user
    if "uniqueid" not in df.columns:
        deviceids = df["deviceid"].unique()
        deviceid_hash_table = {deviceid: create_user_from_deviceid(deviceid) for deviceid in deviceids}
        df["uniqueId"] = df["deviceid"].apply(lambda x: deviceid_hash_table[x][0])
        df["deviceid"] = df["deviceid"].apply(lambda x: deviceid_hash_table[x][1])
    else:
        df = df.rename(columns={"uniqueid": "uniqueId"})
        df["uniqueId"] = df["uniqueId"].apply(lambda x: f"user_{x}")
        unique_ids = df["uniqueId"].unique()
        uniqueId_hash_table = {uniqueId: create_user_from_uniqueId(uniqueId) for uniqueId in unique_ids}
        df["deviceid"] = df["uniqueId"].apply(lambda x: uniqueId_hash_table[x][1])

    # Replay
    # replay_trajectory(df[df["uniqueId"] != "user_39931"], 0.8)
    # replay_trajectory(df, 0.8)
    replay_trajectory_in_pg(df, 0.4)


if __name__ == "__main__":
    main()
    pg_conn.close()
