import csv
from pytz import timezone, utc
from datetime import datetime, timedelta

from common.mongo import get_devices, get_stay_locations
from positions.store import get_uniqueId_from_deviceid


def get_utc_time_from_local_time_str(local_time_str: str, tz: str) -> datetime:
    """
    Get the utc time from the local time string.
    Time string format should be "YYYY-MM-DD HH:MM:SS" or "YYYY-MM-DD".
    """
    local_time = (
        datetime.strptime(local_time_str, "%Y-%m-%d %H:%M:%S")
        if " " in local_time_str
        else datetime.strptime(local_time_str, "%Y-%m-%d")
    )
    kst = timezone(tz)
    local_time = kst.localize(local_time)
    utc_time = local_time.astimezone(utc)
    return utc_time


def get_local_time_str_from_utc_time(utc_time: datetime, tz: str, date_only=False) -> str:
    """
    Get the local time string from the utc time.
    """
    local_tz = timezone(tz)
    local_time = utc_time.astimezone(local_tz)
    return local_time.strftime("%Y-%m-%d %H:%M:%S") if not date_only else local_time.strftime("%Y-%m-%d")


def stay_location_to_dict(stay_location: dict) -> dict:
    """
    Convert the stay location to a dict.
    """
    return {
        "deviceid": stay_location["uid"],
        "uniqueId": stay_location["uniqueId"],
        "datetime": get_local_time_str_from_utc_time(stay_location["datetime"], "Asia/Seoul"),
        "leaving_datetime": get_local_time_str_from_utc_time(stay_location["leaving_datetime"], "Asia/Seoul"),
        "id": stay_location["id"],
        "lat": stay_location["lat"],
        "lng": stay_location["lng"],
        "altitude": stay_location["altitude"],
        "tag": stay_location["tag"],
    }


devices = get_devices()
result = []

for deviceid in devices:
    print(f"Device {deviceid} / {get_uniqueId_from_deviceid(deviceid)}")

    start_date = get_utc_time_from_local_time_str("2022-01-14", "Asia/Seoul")
    end_date = get_utc_time_from_local_time_str("2022-03-23", "Asia/Seoul")
    date = start_date

    while date <= end_date:
        daybegin = date
        dayend = daybegin + timedelta(days=1)

        stay_loations = list(get_stay_locations(deviceid, daybegin, dayend))
        if len(stay_loations) > 0:
            print(
                f"{deviceid} {get_local_time_str_from_utc_time(daybegin, 'Asia/Seoul', date_only=True)} {len(stay_loations)}"
            )
            for loc in stay_loations:
                result.append(stay_location_to_dict(loc))
        date = dayend

with open("stay_locations.csv", "w") as csvfile:
    fieldnames = [
        "deviceid",
        "uniqueId",
        "datetime",
        "leaving_datetime",
        "id",
        "lat",
        "lng",
        "altitude",
        "tag",
    ]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for row in result:
        writer.writerow(row)
